import App from './App.html';

const app = new App({
  target: document.querySelector('.root'),
  data: {
    name: 'jopa'
  }
});

window.app = app;

export default app;
